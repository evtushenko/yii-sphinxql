<?php
/**
 * Connection class file
 *
 * @package sphinxql
 * @author mr.x
 * @since 2013.03.06
 */
namespace sphinxql;
use Yii;
use CLogger;
use CVarDumper;
use CApplicationComponent;
use mysqli;
use mysqli_result;

/**
 * SphinxQL Connection
 *
 * @package sphinxql
 * @author mr.x
 * @since 2013.03.06
 * 
 * @property mysqli $mysqli MySQLi instance
 * @property bool $isConnected Is connected to server
 * @property int $affectedRows Count of affected rows
 * @property string $lastError Last error message
 * @property int $lastErrorNo Last error code
 */
class Connection extends CApplicationComponent
{
	/**
	 * Log category
	 */
	const LOG_CATEGORY = 'sphinxql';

	/**
	 * @var string Server host
	 */
	public $host = 'localhost';
	/**
	 * @var int Server port
	 */
	public $port = 9306;
	/**
	 * @var string Charset
	 */
	public $charset = 'UTF8';
	/**
	 * @var int Max matches
	 * @link http://sphinxsearch.com/docs/current.html#conf-max-matches
	 */
	public $maxMatches = 1000;
	/**
	 * @var bool Enable profiling
	 */
	public $enableProfiling = YII_DEBUG;

	/**
	 * @var mysqli MySQLi connection
	 */
	private $_mysqli;
	/**
	 * @var int Count of affected rows
	 */
	private $_affected_rows = -1;

	/**
	 * @var string Last error message
	 */
	private $_lastError;
	/**
	 * @var int Last error code
	 */
	private $_lastErrorNo;

	/**
	 * Create MySQLi connecting
	 * 
	 * @return mysqli
	 * @throws Exception Connecting error
	 */
	protected function createConnection()
	{
		if ($this->enableProfiling)
		{
			Yii::log(Yii::t('sphinxql', 'Connecting to server "{server}"',
					array('{server}' => $this->host . ':' . $this->port)),
				CLogger::LEVEL_TRACE, static::LOG_CATEGORY);
		}
		
		@$mysqli = new mysqli($this->host, 'root', '', '', $this->port);
		if ($mysqli->connect_errno)
		{
			//last error
			$this->lastError = $mysqli->connect_error;
			$this->setLastErrorNo($mysqli->connect_errno);
			
			Yii::log(Yii::t('sphinxql', 'Error mysqli::connect(): "{error}"',
					array('{error}' => $mysqli->connect_error)),
				CLogger::LEVEL_ERROR, static::LOG_CATEGORY);
			
			throw new Exception($mysqli->connect_error, $mysqli->connect_errno);
		}
		
		if (!$mysqli->select_db('default'))
		{
			//last error
			$this->setLastError($mysqli->error);
			$this->setLastErrorNo($mysqli->errno);
			
			Yii::log(Yii::t('sphinxql', 'Error mysqli::select_db(): "{error}"',
					array('{error}' => $mysqli->error)),
				CLogger::LEVEL_ERROR, static::LOG_CATEGORY);

			throw new Exception($mysqli->error, $mysqli->errno);
		}
		
		if (!$mysqli->set_charset($this->charset))
		{
			//last error
			$this->setLastError($mysqli->error);
			$this->setLastErrorNo($mysqli->errno);
			
			Yii::log(Yii::t('sphinxql', 'Error mysqli::set_charset(): "{error}"',
					array('{error}' => $mysqli->error)),
				CLogger::LEVEL_ERROR, static::LOG_CATEGORY);
			
			throw new Exception($mysqli->error, $mysqli->errno);
		}
		
		return $mysqli;
	}

	/**
	 * Disconnect
	 * 
	 * @return bool
	 */
	public function disconnect()
	{
		if ($this->enableProfiling)
		{
			Yii::log(Yii::t('sphinxql', 'Disconnecting to server "{server}"',
					array('{server}' => $this->host . ':' . $this->port)),
				CLogger::LEVEL_TRACE, static::LOG_CATEGORY);
		}
		
		if ($this->isConnected)
		{
			$this->getMysqli()->close();
			$this->_mysqli = null;
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Get MySQLi connection
	 * 
	 * @return mysqli
	 */
	public function getMysqli()
	{
		if ($this->_mysqli === null)
		{
			$this->_mysqli = $this->createConnection();
		}
		return $this->_mysqli;
	}

	/**
	 * Get is connected
	 * 
	 * @return bool
	 */
	public function getIsConnected()
	{
		return ($this->_mysqli instanceof mysqli);
	}

	/**
	 * Create a new query
	 *
	 * @return Query
	 */
	public function createQuery()
	{
		$query = new Query($this);
		return $query;
	}

	/**
	 * Create a new command
	 *
	 * @param string $text Command text
	 * @return Command
	 */
	public function createCommand($text = '')
	{
		$command = new Command($this);
		return $command->setText($text);
	}

	/**
	 * Get affected rows count
	 *
	 * @return int
	 * @see mysqli::affected_rows
	 */
	public function getAffectedRows()
	{
		return $this->_affected_rows;
	}

	/**
	 * Set affected rows count
	 *
	 * @param int $count Value
	 * @return void
	 */
	public function setAffectedRows($count)
	{
		$this->_affected_rows = (int)$count;
	}

	/**
	 * Get meta-value of last query
	 * 
	 * Example:
	 * <code>
	 * $data = $query->execute();
	 * $count = $query->connection->metaValue('total_found', true);
	 * </code>
	 *
	 * @param string $name Name of variable
	 * @param bool $usePattern
	 * @return string Meta value or NULL
	 *
	 * @see Connection::showMeta()
	 */
	public function metaValue($name, $usePattern = false)
	{
		$meta = $this->showMeta($usePattern ? $name : '');
		return !empty($meta) && isset($meta[$name]) ? $meta[$name] : null;
	}

	/**
	 * Get meta-data of last query
	 *
	 * @param string $pattern LIKE pattern (since sphinx 2.1.1-beta)
	 * @return array|bool Array or FALSE on failure
	 *
	 * @link http://sphinxsearch.com/docs/current.html#sphinxql-show-meta
	 */
	public function showMeta($pattern = '')
	{
		$query = empty($pattern) ?
				"SHOW META" :
				"SHOW META LIKE '" . $this->escapeString($pattern) . "'";
		$cursor = $this->getMysqli()->query($query);

		if ($cursor instanceof mysqli_result)
		{
			$result = array();
			while (($row = $cursor->fetch_array(MYSQL_ASSOC)) !== null)
			{
				$result[$row['Variable_name']] = $row['Value'];
			}
			$cursor->free();
			return $result;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Get warnings
	 *
	 * @return array|bool Array or FALSE on failure
	 *
	 * @link http://sphinxsearch.com/docs/current.html#sphinxql-show-warnings
	 */
	public function showWarnings()
	{
		$cursor = $this->getMysqli()->query("SHOW WARNINGS");

		if ($cursor instanceof mysqli_result)
		{
			$result = array();
			while (($row = $cursor->fetch_array(MYSQL_ASSOC)) !== null)
			{
				$result[] = $row;
			}
			$cursor->free();
			return $result;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Escape string
	 *
	 * @param string $string
	 * @return string
	 *
	 * @see mysqli::real_escape_string()
	 */
	public function escapeString($string)
	{
		return $this->getMysqli()->real_escape_string((string)$string);
	}

	/**
	 * Prepare value to SQL
	 *
	 * @param mixed $value Value
	 * @return string
	 */
	public function prepareValue($value)
	{
		if (is_bool($value))
		{
			return $value ? '1' : '0';
		}
		elseif (is_array($value))
		{
			return '(' . implode(',', array_map(array($this, 'prepareValue'), $value)) . ')';
		}
		elseif (is_int($value) || is_float($value))
		{
			return (string)$value;
		}
		elseif (is_string($value))
		{
			return "'" . $this->escapeString($value) . "'";
		}
		else
		{
			Yii::log(Yii::t('sphinxql', 'Incorrect value type "{type}". Value: "{value}"',
					array('{type}' => gettype($value), '{value}' => CVarDumper::dumpAsString($value))),
				CLogger::LEVEL_WARNING, Connection::LOG_CATEGORY);

			return '';
		}
	}

	/**
	 * Get last error message
	 *
	 * @return string
	 */
	public function getLastError()
	{
		return $this->_lastError;
	}

	/**
	 * Set last error message
	 *
	 * @param string $error Error message
	 * @return void
	 */
	public function setLastError($error)
	{
		$this->_lastError = empty($error) ? null : (string)$error;
	}

	/**
	 * Get last error code
	 * 
	 * @return int
	 */
	public function getLastErrorNo()
	{
		return $this->_lastErrorNo;
	}

	/**
	 * Set last error code
	 * 
	 * @param int $error Error code
	 * @return void
	 */
	public function setLastErrorNo($error)
	{
		$this->_lastErrorNo = empty($error) ? null : (int)$error;
	}
}
