<?php
/**
 * Exception class file
 *
 * @package sphinxql
 * @author mr.x
 * @since 2013.03.06
 */
namespace sphinxql;
use Yii;
use CException;

/**
 * SphinxQL Exception
 *
 * @package sphinxql
 * @author mr.x
 * @since 2013.03.06
 */
class Exception extends CException
{
}
