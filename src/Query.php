<?php
/**
 * Query class file
 *
 * @package sphinxql
 * @author mr.x
 * @since 2013.03.06
 */
namespace sphinxql;
use Yii;
use CLogger;
use CComponent;

/**
 * SphinxQL Query
 * 
 * Example:
 * <code>
 * Yii::app()->sphinxql->createQuery()
 *      ->addIndex('my_index')
 *      ->addSearchCondition('@name a*', false)
 *      ->addCondition('is_visible', true)
 *      ->addInCondition('id', range(1, 1000))
 *      ->limit(10)
 *      ->offset(5)
 *      ->execute();
 * </code>
 *
 * @package sphinxql
 * @author mr.x
 * @since 2013.03.06
 * 
 * @property Connection $connection Sphinx connection
 * @property int $maxMatches Max matches option
 */
class Query extends CComponent
{
	/**
	 * Sort ASC
	 */
	const SORT_ASC = 'ASC';
	/**
	 * Sort DESC
	 */
	const SORT_DESC = 'DESC';

	/**
	 * Ranker "sph04"
	 */
	const RANKER_SPH04 = 'sph04';
	/**
	 * Ranker "bm25"
	 */
	const RANKER_BM25 = 'bm25';
	/**
	 * Ranker "none"
	 */
	const RANKER_NONE = 'none';
	/**
	 * Ranker "proximity"
	 */
	const RANKER_PROXIMITY = 'proximity';
	/**
	 * Ranker "wordcount"
	 */
	const RANKER_WORDCOUNT = 'wordcount';
	/**
	 * Ranker "matchany"
	 */
	const RANKER_MATCHANY = 'matchany';
	/**
	 * Ranker "fieldmask"
	 */
	const RANKER_FIELDMASK = 'fieldmask';
	/**
	 * Ranker "proximity bm25"
	 */
	const RANKER_PROXIMITY_BM25 = 'proximity_bm25';
	
	/**
	 * @var array The indexes that are to be searched
	 */
	protected $_indexes = array();
	/**
	 * @var array The fields that are to be returned in the result set
	 */
	protected $_fields = array();
	/**
	 * @var string A string to be searched for in the indexes
	 */
	protected $_search;
	/**
	 * @var array A set of WHERE conditions
	 */
	protected $_wheres = array();
	/**
	 * @var string The GROUP BY field
	 */
	protected $_group;
	/**
	 * @var string The IN GROUP ORDER BY options
	 */
	protected $_group_order;
	/**
	 * @var array A set of ORDER clauses
	 */
	protected $_orders = array();
	/**
	 * @var int The offset to start returning results from
	 */
	protected $_offset;
	/**
	 * @var int The maximum number of results to return
	 */
	protected $_limit;
	/**
	 * @var array A set of OPTION clauses
	 */
	protected $_options = array();
	/**
	 * @var Connection A reference to a SphinxQL object, used for the execute() function
	 */
	protected $_connection;

	/**
	 * @var int Max Matches
	 */
	private $_maxMatches;

	/**
	 * Constructor
	 *
	 * @param Connection $connection Connection
	 */
	public function __construct(Connection $connection)
	{
		$this->setConnection($connection);
	}

	/**
	 * Magic method __toString()
	 *
	 * @return string
	 * 
	 * @see Query::buildQuery()
	 */
	public function __toString()
	{
		return $this->buildQuery();
	}

	/**
	 * Set the Sphinx connection
	 *
	 * @param Connection $connection Connection
	 * @return void
	 */
	public function setConnection(Connection $connection)
	{
		$this->_connection = $connection;
	}
	
	/**
	 * Get the Sphinx connection
	 *
	 * @return Connection
	 */
	public function getConnection()
	{
		return $this->_connection;
	}

	/**
	 * Escape string
	 *
	 * @param string $string
	 * @return string
	 *
	 * @see Connection::escapeString()
	 */
	public function escapeString($string)
	{
		return $this->getConnection()->escapeString($string);
	}

	/**
	 * Prepare value to SQL
	 *
	 * @param mixed  $value Value
	 * @return string
	 * 
	 * @see Connection::prepareValue()
	 */
	public function prepareValue($value)
	{
		return $this->getConnection()->prepareValue($value);
	}
	

	/**
	 * Builds the query string
	 *
	 * @return string The resulting query
	 */
	public function buildQuery()
	{
		$query = '';

		//fields
		if (count($this->_fields) > 0)
		{
			$fields = array();
			foreach ($this->_fields as  $alias => $field)
			{
				if ($alias === $field)
				{
					$fields[] = $field;
				}
				else
				{
					$fields[] = $field . ' AS ' . $alias;
				}
			}
			$query .= 'SELECT ' . implode(', ', $fields). '';
			unset($fields);
		}
		else
		{
			$query .= 'SELECT *';
		}
		
		//indexes
		if (count($this->_indexes) > 0)
		{
			$query .= ' FROM ' . implode(', ', array_keys($this->_indexes));
		}
		else
		{
			$query .= ' FROM index';
		}

		//where
		if (count($this->_wheres) > 0 || $this->_search !== null)
		{
			//simple conditions
			$wheres = $this->_wheres;
			
			//full-text condition
			if ($this->_search !== null)
			{
				$wheres[] = "MATCH('" . $this->_search . "')";
			}
			
			$query .= ' WHERE ' . implode(' AND ', $wheres);
			unset($wheres);
		}
		
		//group by
		if ($this->_group !== null)
		{
			$query .= ' GROUP BY ' . $this->_group;
		}
		
		//within group order by
		if ($this->_group_order !== null)
		{
			$query .= ' WITHIN GROUP ORDER BY ' . $this->_group_order;
		}
		
		//order by
		if (count($this->_orders) > 0)
		{
			$orders = array();
			foreach ($this->_orders as $field => $sort)
			{
				$orders[] = $field . ' ' . $sort;
			}
			
			$query .= ' ORDER BY ' . implode(', ', $orders);
			unset($orders);
		}
		
		//limit & ooset
		if ($this->_limit !== null)
		{
			$query .= ' LIMIT ' . (int)$this->_offset . ',' . (int)$this->_limit;
		}
		
		//options
		if (count($this->_options) > 0)
		{
			$options = array();
			foreach ($this->_options as $name => $value)
			{
				$options[] = $name . '=' . $value;
			}

			$query .= ' OPTION ' . implode(', ', $options);
			unset($options);
		}

		return $query;
	}

	/**
	 * Add index
	 *
	 * @param string $index The index to add
	 * @return Query This object
	 */
	public function addIndex($index)
	{
		$this->_indexes[$index] = true;
		return $this;
	}

	/**
	 * Removes an entry from the list of indexes to be searched.
	 *
	 * @param string $index The index to remove
	 * @return Query This object
	 */
	public function removeIndex($index)
	{
		unset($this->_indexes[$index]);
		return $this;
	}

	/**
	 * Add field
	 *
	 * @param string $field Field to add
	 * @param string $alias Alias for that field, optional
	 * @return Query This object
	 */
	public function addField($field, $alias = null)
	{
		if (empty($alias))
		{
			$alias = $field;
		}
		
		$this->_fields[$alias] = $field;
		return $this;
	}

	/**
	 * Add multiple fields
	 * 
	 * Format:
	 * <code>array('field' => 'alias', 'field', ...)</code>
	 * The alias is optional.
	 *
	 * @param array Array of fields to add
	 * @return Query This object
	 */
	public function addFields($fields)
	{
		foreach ($fields as $field => $alias)
		{
			if (is_int($field))
			{
				$field = $alias;
				$alias = null;
			}
			
			$this->addField($field, $alias);
		}
		return $this;
	}

	/**
	 * Set field list
	 *
	 * @param array Array of fields to add
	 * @return Query This object
	 * @see Query::addFields()
	 */
	public function setFields($fields)
	{
		$this->_fields = array();
		$this->addFields($fields);
		return $this;
	}

	/**
	 * Remove a field from the list of fields to search
	 *
	 * @param string $alias Alias of the field to remove
	 * @return Query This object
	 */
	public function removeField($alias)
	{
		unset($this->_fields[$alias]);
		return $this;
	}

	/**
	 * Remove multiple fields at once from the list of fields to search.
	 *
	 * @param array $fields List of aliases of fields to remove
	 * @return Query This object
	 * 
	 * @see Query::removeField()
	 */
	public function removeFields($fields)
	{
		foreach ($fields as $alias)
		{
			$this->removeField($alias);
		}
		return $this;
	}

	/**
	 * Add search condition
	 *
	 * @param string $search Text to be searched
	 * @param bool $escape Escape string
	 * @return Query This object
	 * 
	 * @link http://sphinxsearch.com/docs/current.html#extended-syntax
	 */
	public function addSearchCondition($search, $escape = true)
	{
		$this->_search = empty($search) ? null :
				($escape ? $this->escapeString($search) : $search);
		return $this;
	}

	/**
	 * Remove search condition
	 *
	 * @return Query This object
	 */
	public function removeSearch()
	{
		$this->_search = null;
		return $this;
	}

	/**
	 * Set OFFSET
	 *
	 * @param int $offset Offset
	 * @return Query This object
	 */
	public function offset($offset)
	{
		$this->_offset = $offset === null ? null : (int)$offset;
		return $this;
	}

	/**
	 * Set LIMIT
	 *
	 * @param int $limit Limit
	 * @return Query This object
	 */
	public function limit($limit)
	{
		$this->_limit = $limit === null ? null : (int)$limit;
		return $this;
	}

	/**
	 * Set field weights
	 * 
	 * Format:
	 * <code>array('field' => weight, ...)</code>
	 * 
	 * @param array $weights Field weights
	 * @return Query This object
	 * 
	 * @see Query::addOption()
	 * @link http://sphinxsearch.com/docs/current.html#weighting
	 */
	public function setFieldWeights($weights)
	{
		$value = array();
		foreach ($weights as $field => $weight)
		{
			$value[] = $field . '=' . $weight;
		}
		
		if (count($value) > 0)
		{
			$this->addOption('field_weights', '(' . implode(',', $value) . ')');
		}
		else
		{
			$this->removeOption('field_weights');
		}
		
		return $this;
	}

	/**
	 * Set ranker type
	 * 
	 * @param string $ranker Ranker type
	 * @return Query This object
	 * 
	 * @see Query::addOption()
	 * @link http://sphinxsearch.com/docs/current.html#weighting
	 */
	public function setRanker($ranker = self::RANKER_BM25)
	{
		$this->addOption('ranker', $ranker);
		return $this;
	}
	
	/**
	 * Set "max matches" option
	 *
	 * @param int $maxMatches Max matches
	 * @return Query This object
	 * 
	 * @see Query::addOption()
	 */
	public function setMaxMatches($maxMatches = 1000)
	{
		$this->_maxMatches = $maxMatches > 0 ? null : (int)$maxMatches;
		if ($this->_maxMatches === null)
		{
			$this->addOption('max_matches', $maxMatches);
		}
		else
		{
			$this->removeOption('max_matches');
		}
		return $this;
	}

	/**
	 * Get "max matches" option
	 * 
	 * @return int
	 */
	public function getMaxMatches()
	{
		if ($this->_maxMatches === null)
		{
			return $this->getConnection()->maxMatches;
		}
		return $this->_maxMatches;
	}
	
	/**
	 * Add field condition
	 *
	 * @param string $field The field/expression for the condition
	 * @param string $value The field/expression/value to compare the field to
	 * @param string $operator The operator (=, !=, <, <=, >, >=)
	 * @param bool $escape Escape value
	 * @return Query This object
	 * @throws Exception If unknown operator
	 */
	public function addCondition($field, $value, $operator = '=', $escape = true)
	{
		if (!in_array($operator, array('=', '!=', '>', '<', '>=', '<='), true))
		{
			throw new Exception(Yii::t('sphinxql', 'Unknown operator "{operator}" in method "{method}"',
					array('{operator}' => $operator, '{method}' => __METHOD__)),
				CLogger::LEVEL_WARNING, Connection::LOG_CATEGORY);
		}
		
		if ($escape)
		{
			$value = $this->prepareValue($value);
		}
		
		$this->_wheres[] = $field . ' ' . $operator . ' ' . $value;
		return $this;
	}

	/**
	 * Add (NOT)IN condition
	 *
	 * @param string $field The field/expression for the condition
	 * @param array $values The values to compare the field to
	 * @param bool $exclude Exclude or include values
	 * @return Query This object
	 */
	public function addInCondition($field, $values, $exclude = false)
	{
		$this->_wheres[] = $field . ' ' .
				($exclude ? 'NOT IN' : 'IN') . ' ' .
				$this->prepareValue(is_array($values) ? $values : array($values));
		
		return $this;
	}

	/**
	 * Add BETWEEN condition
	 *
	 * Example:
	 * <code>
	 * //WHERE id BETWEEN 5 AND 10
	 * $query->addBetweenCondition('id', 5, 10);
	 * <code>
	 *
	 * @param string $field The field/expression for the condition
	 * @param int $min Min value
	 * @param int $max Max value
	 * @return Query This object
	 */
	public function addBetweenCondition($field, $min, $max)
	{
		$this->_wheres[] = $field . ' BETWEEN ' .
				$this->prepareValue($min) . ' AND ' . $this->prepareValue($max);

		return $this;
	}

	/**
	 * Set WHERE statement
	 *
	 * @param string|array $conditions List of conditions. Reset where, if empty
	 * @return Query This object
	 */
	public function setWhere($conditions)
	{
		if (empty($conditions))
		{
			$this->_wheres = array();
		}
		elseif (is_string($conditions))
		{
			$this->_wheres = array($conditions);
		}
		else
		{
			$this->_wheres = (array)$conditions;
		}
		return $this;
	}

	/**
	 * Add WHERE condition
	 * 
	 * @param string $condition Condition, e.g. "is_active = 1"
	 * @return Query This object
	 */
	public function addWhere($condition)
	{
		$this->_wheres[] = (string)$condition;
	}

	/**
	 * Set GROUP BY condition
	 *
	 * @param string $field The field/expression for the condition
	 * @return Query This object
	 */
	public function groupBy($field)
	{
		$this->_group = empty($field) ? null : (string)$field;
		return $this;
	}

	/**
	 * Remove the GROUP BY condition
	 * 
	 * @return Query This object
	 */
	public function removeGroupBy()
	{
		$this->_group = null;
		return $this;
	}

	/**
	 * Add ORDER BY condition
	 *
	 * @param string $field The field/expression for the condition
	 * @param string $sort The sort type ('ASC' or 'DESC')
	 * @return Query This object
	 * 
	 * @link http://sphinxsearch.com/docs/current.html#sphinxql-select
	 */
	public function addOrderBy($field, $sort = self::SORT_ASC)
	{
		$this->_orders[$field] = $sort;
		return $this;
	}

	/**
	 * Remove ORDER BY condition
	 *
	 * @param string $field The field/expression for the condition
	 * @return Query This object
	 */
	public function removeOrderBy($field)
	{
		unset($this->_orders[$field]);
		return $this;
	}

	/**
	 * Set ORDER BY condition
	 *
	 * @param array $fields The fields array(field => sort, field, ...)
	 * @return Query This object
	 * 
	 * @see Query::addOrderBy()
	 */
	public function setOrderBy($fields)
	{
		$this->_orders = array();
		foreach ($fields as $field => $sort)
		{
			if (is_int($field))
			{
				$field = $sort;
				$sort = self::SORT_ASC;
			}
			$this->addOrderBy($field, $sort);
		}
		return $this;
	}

	/**
	 * Set WITHIN GROUP ORDER BY condition
	 * 
	 * This is a Sphinx-specific extension to SQL.
	 *
	 * @param string $field The field/expression for the condition
	 * @param string $sort The sort type ('ASC' or 'DESC')
	 * @return Query This object
	 * 
	 * @link http://sphinxsearch.com/docs/current.html#sphinxql-select
	 */
	public function groupOrder($field, $sort = self::SORT_ASC)
	{
		$this->_group_order = $field . ' ' . $sort;
		return $this;
	}

	/**
	 * Remove the WITHIN GROUP ORDER BY condition for the query.
	 * 
	 * This is a Sphinx-specific extension to SQL.
	 *
	 * @return Query This object
	 */
	public function removeGroupOrder()
	{
		$this->_group_order = null;
		return $this;
	}

	/**
	 * Add OPTION to the query
	 * 
	 * This is a Sphinx-specific extension to SQL.
	 *
	 * @param string $name The option name
	 * @param string $value The option value
	 * @return Query This object
	 * 
	 * @link http://sphinxsearch.com/docs/current.html#sphinxql-select
	 */
	public function addOption($name, $value)
	{
		$this->_options[$name] = $value;
		return $this;
	}

	/**
	 * Remove OPTION from the query.
	 *
	 * @param string $name The option name
	 * @return Query This object
	 */
	public function removeOption($name)
	{
		unset($this->_options[$name]);
		return $this;
	}

	/**
	 * Execute the query and return the results
	 *
	 * @return array Results of the query
	 * 
	 * @see Command::query()
	 */
	public function execute()
	{
		return $this->getConnection()->createCommand()->query($this);
	}
}