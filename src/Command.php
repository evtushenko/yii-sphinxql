<?php
/**
 * Command class file
 * 
 * @package sphinxql
 * @author mr.x
 * @since 2013.03.07
 */
namespace sphinxql;
use Yii;
use CLogger;
use CComponent;
use CVarDumper;
use mysqli_result;

/**
 * SphinxQL Command
 *
 * @package sphinxql
 * @author mr.x
 * @since 2013.03.07
 * 
 * Example:
 * <code>
 * Yii::app()->sphinxql->createCommand()->update
 * (
 *      'my_index',
 *      array
 *      (
 *          'is_visible' => true,
 *          'updated' => time(),
 *          'children' => array(1,2,3),
 *      ),
 *      "MATCH('abc') AND id BETWEEN 50 AND 100"
 * );
 * </code>
 * 
 * @property int $affectedRows Count of affected rows
 * @property string $text Command text
 * @property Connection $connection Sphinx connection
 */
class Command extends CComponent
{
	/**
	 * @var Connection Sphinx connection
	 */
	private $_connection;
	/**
	 * @var int Count of affected rows
	 */
	private $_affected_rows = -1;
	/**
	 * @var string Command text
	 */
	private $_text = '';
	
	/**
	 * Constructor
	 *
	 * @param Connection $connection Connection
	 */
	public function __construct(Connection $connection)
	{
		$this->setConnection($connection);
	}

	/**
	 * Set Sphinx connection
	 *
	 * @param Connection $connection Connection
	 * @return void
	 */
	public function setConnection(Connection $connection)
	{
		$this->_connection = $connection;
	}

	/**
	 * Get Sphinx connection
	 *
	 * @return Connection
	 */
	public function getConnection()
	{
		return $this->_connection;
	}

	/**
	 * Get command text
	 * 
	 * @return string
	 */
	public function getText()
	{
		return $this->_text;
	}

	/**
	 * Set command text
	 * 
	 * @param string $text Text
	 * @return Command This object
	 */
	public function setText($text)
	{
		$this->_text = (string)$text;
		return $this;
	}
	
	/**
	 * Perform a query, given either a string or a SphinxQL\Query object
	 *
	 * @param Query|string A query as a string or a SphinxQL\Query object
	 * @param int $resultType Result type
	 * @return array|bool The result of the query
	 */
	public function query($query, $resultType = MYSQLI_ASSOC)
	{
		$text = (string)$query;
		
		$start = microtime(true);
		$cursor = $this->setText($text)->processQuery($text);

		$result = array();
		if ($cursor instanceof mysqli_result)
		{
			while (($row = $cursor->fetch_array($resultType)) !== null)
			{
				$result[] = $row;
			}
		}

		if ($this->getConnection()->enableProfiling)
		{
			Yii::log(Yii::t('sphinxql', 'Quering "{query}". Elapsed: {elapsed}.',
					array('{query}' => $text, '{elapsed}' => sprintf('%.6f s', microtime(true) - $start))),
				CLogger::LEVEL_INFO, Connection::LOG_CATEGORY);
		}

		return $result;
	}

	/**
	 * Perform a query
	 *
	 * @return bool The result of the query
	 */
	public function execute()
	{
		$text = $this->getText();
		
		$start = microtime(true);
		$result = $this->processQuery($text);

		if ($this->getConnection()->enableProfiling)
		{
			Yii::log(Yii::t('sphinxql', 'Executing "{query}". Elapsed: {elapsed}',
					array('{query}' => $text, '{elapsed}' => sprintf('%.6f s', microtime(true) - $start))),
				CLogger::LEVEL_INFO, Connection::LOG_CATEGORY);
		}

		return $result ? true : false;
	}

	/**
	 * Escape string
	 *
	 * @param string $string
	 * @return string
	 *
	 * @see Connection::escapeString()
	 */
	public function escapeString($string)
	{
		return $this->getConnection()->escapeString($string);
	}

	/**
	 * Prepare value to SQL
	 *
	 * @param mixed $value Value
	 * @return string
	 * 
	 * @see Connection::prepareValue()
	 */
	public function prepareValue($value)
	{
		return $this->getConnection()->prepareValue($value);
	}

	/**
	 * Get affected rows count
	 *
	 * @return int
	 * @see mysqli::affected_rows
	 */
	public function getAffectedRows()
	{
		return $this->_affected_rows;
	}

	/**
	 * Set affected rows count
	 *
	 * @param int $count Value
	 * @return void
	 */
	public function setAffectedRows($count)
	{
		$this->_affected_rows = (int)$count;
	}

	/**
	 * Process query
	 *
	 * @param string $query Query
	 * @return mysqli_result|bool
	 * @throws Exception If query is empty
	 */
	protected function processQuery($query)
	{
		if (empty($query))
		{
			throw new Exception(Yii::t('sphinxql', 'Executing empty query'));
		}

		$mysqli = $this->getConnection()->getMysqli();
		$result = $mysqli->query($query);
		
		//affected rows
		$this->setAffectedRows($mysqli->affected_rows);
		$this->getConnection()->setAffectedRows($this->_affected_rows);

		if ($mysqli->errno)
		{
			//last error
			$this->getConnection()->setLastError($mysqli->error);
			$this->getConnection()->setLastErrorNo($mysqli->errno);
			
			Yii::log(Yii::t('sphinxql', 'Error mysqli::query(): "{error}"',
					array('{error}' => $mysqli->error)),
				CLogger::LEVEL_ERROR, Connection::LOG_CATEGORY);

			return false;
		}

		//log warnings
		if ($this->getConnection()->enableProfiling)
		{
			$warnings = $this->getConnection()->showWarnings();
			if (!empty($warnings))
			{
				Yii::log(Yii::t('sphinxql', 'Warnings mysqli::query(): "{warning}"',
						array('{warning}' => CVarDumper::dumpAsString($warnings))),
					CLogger::LEVEL_WARNING, Connection::LOG_CATEGORY);
			}
		}

		return $result;
	}

	/**
	 * Alias of query()
	 *
	 * @param Query|string $query Query
	 * @param int $resultType Result type
	 * @return array|bool Query result
	 *
	 * @see Connection::query()
	 */
	public function select($query, $resultType = MYSQLI_ASSOC)
	{
		return $this->query($query, $resultType);
	}

	/**
	 * Delete rows
	 *
	 * @param string $index Index name
	 * @param int|array $id List of ids
	 * @return int|bool Count of deleted rows or FALSE on failure
	 *
	 * @link http://sphinxsearch.com/docs/current.html#sphinxql-delete
	 */
	public function delete($index, $id)
	{
		if (is_array($id))
		{
			$values = implode(',', array_map('intval', $id));
			$query = 'DELETE FROM ' .$index . ' WHERE id IN (' . $values . ')';
		}
		else
		{
			$value = (int)$id;
			$query = 'DELETE FROM ' . $index . ' WHERE id = ' . $value;
		}

		$result = $this->setText($query)->execute();
		return $result ? $this->affectedRows : false;
	}

	/**
	 * Insert row
	 *
	 * @param string $index Index name
	 * @param array $values Values array(field => value)
	 * @return bool
	 * 
	 * @see Command::createInsertStatement()
	 */
	public function insert($index, $values)
	{
		$text = $this->createInsertStatement($index, $values, false);
		return $this->setText($text)->execute();
	}

	/**
	 * Replace row
	 *
	 * @param string $index Index name
	 * @param array $values Values array(field => value)
	 * @return bool Result
	 * 
	 * @see Command::createInsertStatement()
	 */
	public function replace($index, $values)
	{
		$text = $this->createInsertStatement($index, $values, true);
		return $this->setText($text)->execute();
	}

	/**
	 * Update
	 * 
	 * @param string $index Index name
	 * @param array $values Values (field => value, ...)
	 * @param string|array $conditions Where condition(s)
	 * @param array $options
	 * @return int|bool
	 * 
	 * @link http://sphinxsearch.com/docs/current.html#sphinxql-update
	 */
	public function update($index, $values, $conditions, $options = array())
	{
		$text = 'UPDATE ' . $index;

		//new values
		$v = array();
		foreach ($values as $field => $value)
		{
			$v[] = $field . '=' . $this->prepareValue($value);
		}
		$text .= ' SET ' . implode(', ', $v);
		unset($v);

		//conditions
		if (is_array($conditions))
		{
			$conditions = implode(' AND ', $conditions);
		}
		$text .= ' WHERE ' . $conditions;

		//options
		if (count($options) > 0)
		{
			$o = array();
			foreach ($options as $option => $value)
			{
				$o[] = $option . '=' .$value;
			}
			$text .= ' OPTION ' . implode(',', $o);
			unset($o);
		}

		$result = $this->setText($text)->execute();
		return $result ? $this->affectedRows : false;
	}

	/**
	 * Create insert or replace statement
	 *
	 * @param string $index Index name
	 * @param array $values Values array(field => value)
	 * @param bool $replace Use REPLACE
	 * @return string Query string
	 *
	 * @link http://sphinxsearch.com/docs/2.0.4/sphinxql-insert.html
	 */
	protected function createInsertStatement($index, $values, $replace = false)
	{
		if (array_keys($values) === range(0, count($values)))
		{
			$values = array_values($values);
			$names = array();
		}
		else
		{
			$names = array_keys($values);
			$values = array_values($values);
		}

		//prepare values
		$values = array_map(array($this, 'prepareValue') , $values);

		//insert into
		$query = ($replace ? 'REPLACE' : 'INSERT') . ' INTO ' . $index;
		//fields
		if (count($names) > 0)
		{
			$query .= ' (' . implode(',', $names) . ')';
		}
		//values
		$query .= ' VALUES (' . implode(',', $values) . ')';

		//
		return $query;
	}

	/**
	 * Build excerpts
	 * 
	 * @param string|array $text Text or array of text string
	 * @param string $index Index name
	 * @param string $query Query
	 * @param array $options Options
	 * @return array|string|bool Array or string snippets. False on failure
	 * 
	 * @link http://sphinxsearch.com/docs/2.0.4/sphinxql-call-snippets.html
	 * @link http://sphinxsearch.com/docs/2.0.4/api-func-buildexcerpts.html
	 */
	public function buildExcerpts($text, $index, $query, $options = array())
	{
		$args = array();
		
		//data
		if (is_array($text))
		{
			$list = array();
			foreach ($text as $value)
			{
				$list[] = $this->prepareValue((string)$value);
			}
			$args[] = '(' . implode(',', $list) . ')';
			unset($list, $value);
		}
		else
		{
			$args[] = $this->prepareValue((string)$text);
		}
		
		//index
		$args[] = $this->prepareValue((string)$index);
		
		//query
		$args[] = $this->prepareValue($query);
		
		//options
		if (count($options) > 0)
		{
			foreach ($options as $option => $value)
			{
				$args[] = $this->prepareValue($value) . ' AS ' . $option;
			}
			unset($option, $value);
		}
		
		//sql
		$command = 'CALL SNIPPETS(' . implode(', ', $args) . ')';
		$data = $this->query($command);
		
		//success
		if (is_array($data) && count($data) > 0)
		{
			if (is_array($text))
			{
				$result = array();
				foreach ($data as $row)
				{
					$result[] = $row['snippet'];
				}
				return $result;
			}
			else
			{
				return $data[0]['snippet'];
			}
		}
		else
		{
			return false;
		}
	}
}
