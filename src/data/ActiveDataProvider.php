<?php
/**
 * ActiveDataProvider class file
 *
 * @package sphinxql\data
 * @author mr.x
 * @since 2013.03.06
 */
namespace sphinxql\data;
use CActiveRecord;
use sphinxql\Query;

/**
 * SphinxQL ActiveDataProvider
 *
 * @package sphinxql\data
 * @author mr.x
 * @since 2013.03.06
 */
class ActiveDataProvider extends DataProvider
{
	/**
	 * @var CActiveRecord Active record model
	 */
	private $_finder;

	/**
	 * Constructor
	 *
	 * @param CActiveRecord $finder Finder
	 * @param Query $query Sphinx query
	 * @param array $config Params
	 */
	public function __construct(CActiveRecord $finder, Query $query, $config = array())
	{
		$this->setFinder($finder);
		parent::__construct($query, $config);
	}

	/**
	 * Set finder
	 *
	 * @param CActiveRecord $finder 
	 * @return void
	 */
	public function setFinder(CActiveRecord $finder)
	{
		$this->_finder = $finder;
	}
	
	/**
	 * Get finder
	 * 
	 * @return CActiveRecord
	 */
	public function getFinder()
	{
		return $this->_finder;
	}

	/**
	 * @inheritDoc
	 * @return CActiveRecord[]
	 */
	protected function fetchData()
	{
		//find raw data
		$raw = parent::fetchData();

		//found none
		if (count($raw) < 1)
		{
			return array();
		}
		
		//ids list
		$ids = array_map(function($row)
		{
			return (int)$row['id'];
		}, $raw);
		$ids = implode(',', $ids);

		//create criteria
		$alias = $this->getFinder()->getTableAlias();
		$field = $this->getFinder()->getTableSchema()->primaryKey;
		$criteria = array
		(
			'condition' => "{$alias}.{$field} IN ({$ids})",
			'order' => "FIELD({$alias}.{$field}, {$ids})",
			'offset' => -1,
			'limit' => -1,
		);
		
		//find all
		return $this->getFinder()->findAll($criteria);
	}
}
