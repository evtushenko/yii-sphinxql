<?php
/**
 * DataProvider class file
 *
 * @package sphinxql\data
 * @author mr.x
 * @since 2013.03.06
 */
namespace sphinxql\data;
use Yii;
use CSort;
use CDataProvider;
use sphinxql\Query;

/**
 * SphinxQL DataProvider
 *
 * @package sphinxql\data
 * @author mr.x
 * @since 2013.03.06
 * 
 * @property Query $query Sphinx query
 */
class DataProvider extends CDataProvider
{
	/**
	 * @var string Field Key
	 */
	public $keyField = 'id';
	
	/**
	 * @var Query Sphinx query
	 */
	private $_query;
	
	/**
	 * @var array Query result
	 */
	private $_data;
	/**
	 * @var int Total item count
	 */
	private $_count;

	/**
	 * Constructor
	 *
	 * @param Query $query Sphinx query
	 * @param array $config Params
	 */
	public function __construct(Query $query, $config = array())
	{
		$this->setQuery($query);
		foreach ($config as $key => $value)
		{
			$this->{$key} = $value;
		}
	}

	/**
	 * Set sphinx query
	 *
	 * @param Query $query
	 * @return void
	 */
	public function setQuery(Query $query)
	{
		$this->_query = $query;
		$this->_count = $this->_data = null;
	}

	/**
	 * Get sphinx query
	 * 
	 * @return Query
	 */
	public function getQuery()
	{
		return $this->_query;
	}
	
	/**
	 * @inheritDoc
	 */
	protected function fetchKeys()
	{
		if ($this->keyField === false)
		{
			return array_keys($this->data);
		}
		
		$keys = array();
		foreach ($this->data as $i => $data)
		{
			$keys[$i] = is_object($data) ? $data->{$this->keyField} : $data[$this->keyField];
		}
		return $keys;
	}

	/**
	 * @inheritDoc
	 */
	public function calculateTotalItemCount()
	{
		if ($this->_count === null)
		{
			$this->processQuery();
		}
		return $this->_count;
	}

	/**
	 * @inheritDoc
	 */
	protected function fetchData()
	{
		if ($this->_data === null)
		{
			$this->processQuery();
		}
		return $this->_data;
	}

	/**
	 * Process query
	 * 
	 * @return void
	 */
	protected function processQuery()
	{
		//pagination
		$pagination = $this->getPagination();
		if ($pagination !== false)
		{
			$limit = $pagination->pageSize;
			$offset = ($this->calculateCurrentPage() - 1) * $limit;
			
			//apply limit
			$this->getQuery()->limit($limit)->offset($offset);
		}

		//sorting
		$sort = $this->getSort();
		if ($sort !== false)
		{
			$directions = $this->parseSort($sort);
			foreach ($directions as $field => $sort)
			{
				$this->getQuery()->addOrderBy($field, $sort);
			}
		}

		//load data
		$data = $this->getQuery()->execute();
		if (is_array($data))
		{
			//load item count
			$meta = $this->getQuery()->getConnection()->showMeta();
			$totalItemCount = isset($meta['total_found']) ? (int)$meta['total_found'] : 0;
		}
		else
		{
			$data = array();
			$totalItemCount = 0;
		}

		//update pagination
		if ($pagination !== false)
		{
			$pagination->setItemCount($totalItemCount);
		}

		//store
		$this->_data = $data;
		$this->_count = $totalItemCount;
	}

	/**
	 * Calculate current page number
	 * 
	 * @return int
	 * @see Query::getMaxMatches()
	 */
	protected function calculateCurrentPage()
	{
		$pagination = $this->getPagination();
		if ($pagination !== false)
		{
			$curPage = isset($_GET[$pagination->pageVar]) ? (int)$_GET[$pagination->pageVar] : 1;
			if ($curPage < 1)
			{
				return 1;
			}
			
			$max = floor($this->getQuery()->getMaxMatches() / $pagination->pageSize) + 1;
			if ($curPage > $max)
			{
				return (int)$max;
			}
			else
			{
				return $curPage;
			}
		}
		else
		{
			return 1;
		}
	}

	/**
	 * Parse sort params
	 * 
	 * @param CSort $sort
	 * @return array
	 */
	protected function parseSort($sort)
	{
		$orderBy = $sort->getOrderBy();
		if (empty($orderBy))
		{
			return array();
		}
		
		$result = array();
		
		$orderBy = preg_split('/\s*,\s*/', $orderBy, -1, PREG_SPLIT_NO_EMPTY);
		foreach ($orderBy as $expression)
		{
			$chunks = preg_split('/\s+/', $expression, 2, PREG_SPLIT_NO_EMPTY);
			if (count($chunks) === 1)
			{
				$field = $chunks[0];
				$direction = Query::SORT_ASC;
			}
			else
			{
				$field = $chunks[0];
				$direction = strcasecmp($chunks[1], 'desc') === 0 ? Query::SORT_DESC : Query::SORT_ASC;
			}
			$result[$field] = $direction;
		}
		return $result;
	}
}
