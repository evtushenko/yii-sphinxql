Requirements
------------
 * Yii 1.1+
 * Sphinx 2.0.1+ with [enabled mysql protocol](http://sphinxsearch.com/docs/current.html#conf-listen)

Installation
------------
Extract the extension package to `protected/extensions` directory of your application
Register "sphinxql" namespace:
```php
Yii::setPathOfAlias('sphinxql', $pathToExtensionsDir . '/sphinxql');
```
Register SphinxQL application component:
```php
return array
(
    //...
    'components' => array
    (
        //...
        'sphinxql' => array
        (
            'class'             => 'sphinxql\Connection',
            'host'              => '127.0.0.1',
            'port'              => 9306,
            'enableProfiling'   => YII_DEBUG,
        ),
        //...
    ),
    //...
);
```

Examples
--------
Searching:
```php
$query = Yii::app()->sphinxql->createQuery()
    ->addIndex('my_index')
    ->addSearchCondition('@name a*', false)
    ->addCondition('is_visible', true)
    ->addInCondition('id', range(1, 1000))
    ->limit(10)
    ->offset(5);
$data = $query->execute();
$count = (int)$query->connection->metaValue('total_found', true);
```

Updating:
```php
$affected = Yii::app()->sphinxql->createCommand()->update
(
    'my_index',
    array
    (
        'is_visible' => true,
        'updated' => time(),
        'children' => array(1,2,3),
    ),
    "MATCH('abc') AND id BETWEEN 50 AND 100"
);
```

Resources
---------
 * [Yii framework](http://www.yiiframework.com)
 * [Sphinx search](http://sphinxsearch.com)
 * [SphinxQL reference](http://sphinxsearch.com/docs/current.html#sphinxql-reference)